<?php

/**
 * @file
 * API documentation for the Eloqua Asynchronous Visitor Tracking module.
 */

/**
 * Alter the URL tracked by Eloqua for the current page.
 *
 * @param string $url
 *   The URL to track for the current page.
 * @param string $referer
 *   The referrer to track for the current page.
 * @param string $site
 *   The ID of the site.
 */
function hook_alter_eloqua_tracking_url(&$url, &$referer, $site) {

}

/**
 * Alter the _elqQ JavaScript array.
 *
 * @param array $_elqQ
 *   An array of arrays.
 */
function hook_alter_elqQ(&$_elqQ) {

}
