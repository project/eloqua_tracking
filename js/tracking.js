var _elqQ = _elqQ || [];
Drupal.Eloqua = Drupal.Eloqua || {};
(function($){
  // Retrieve the _elqQ array from Drupal's settings.
  $.extend(_elqQ, Drupal.settings._elqQ);
  // Asynchronous loading of elqCfg.min.js.
  function elq_async_load() {
    var s = document.createElement('script'); s.type = 'text/javascript';
    s.async = true; s.src = '//img.en25.com/i/elqCfg.min.js';
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
  }
  if (window.addEventListener) {
    window.addEventListener('DOMContentLoaded', elq_async_load, false);
  }
  else if (window.attachEvent) {
    window.attachEvent('onload', elq_async_load);
  }

  /**
   * Retrieves the customer GUID.
   *
   * @param callback(err, guid)
   *   The function that will be executed on every object.
   */
  Drupal.Eloqua.getCustomerGuid = function(callback) {
    if (typeof GetElqCustomerGUID === 'function') {
      // GUID is available, invoke callback without error.
      callback(null, GetElqCustomerGUID());
    }
    else {
      // Request the customer GUID and wait for it.
      if (typeof Drupal.Eloqua.getCustomerGuid.timeout === 'undefined') {
        // No pending elqGetCustomerGUID, push one.
        _elqQ.push(['elqGetCustomerGUID']);
        // Reset timeout counter.
        Drupal.Eloqua.getCustomerGuid.timeout = 5;
      }
      else {
        // Pending elqGetCustomerGUID, decrease timeout counter.
        Drupal.Eloqua.getCustomerGuid.timeout -= 1;
      }
      if (Drupal.Eloqua.getCustomerGuid.timeout != 0) {
        // The customer GUID is not there yet, wait a bit more.
        setTimeout(Drupal.Eloqua.getCustomerGuid.bind(this, callback), 500);
      }
      else {
        // Timeout while waiting for the customer GUID, invoke callback with error.
        callback(new Error("Timeout while waiting for the Eloqua customer GUID."), null);
      }
    }
  };

  Drupal.behaviors.EloquaTracking = {
    'attach': function(context, settings) {
      // Fill-in elqCustomerGUID inputs element in any forms.
      var forms = $('form', context).has('input[name="elqCustomerGUID"]');
      if (forms.length) {
        Drupal.Eloqua.getCustomerGuid(function(err, guid) {
          if (!err) {
            forms.find('input[name="elqCustomerGUID"]').val(guid);
          }
        });
      }
    }
  };
})(jQuery);