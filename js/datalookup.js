Drupal.Eloqua = Drupal.Eloqua || {};
(function(){
  /**
   * Simple function to performs an Eloqua Data Lookup.
   *
   * @param key
   *   The Data Lookup Key.
   * @param value
   *   (optional) The Data Lookup value.
   * @param callback
   *   The callback function to execute when the data lookup is completed. The function
   *   receive the GetElqContentPersonalizationValue function as argument.
   */
  Drupal.Eloqua.dataLookup = function(key, value, callback) {
    if (typeof callback === 'undefined') {
      callback = value;
      value = '';
    }
    Drupal.Eloqua.dataLookup.queue.push([key, value, callback, false]);
    if (Drupal.Eloqua.dataLookup.queue.length === 1) {
      _elqQ.push(['elqDataLookup', escape(key), value]);
    }
  };
  Drupal.Eloqua.dataLookup.queue = [];

  /**
   * Callback function for Eloqua Data Lookups.
   */
  window.SetElqContent = function() {
    var lookup = Drupal.Eloqua.dataLookup.queue.shift();
    if (typeof window.GetElqContentPersonalizationValue !== 'function') {
      window.GetElqContentPersonalizationValue = function() { return '';};
    }
    if (Drupal.Eloqua.dataLookup.queue.length) {
      _elqQ.push(['elqDataLookup', escape(Drupal.Eloqua.dataLookup.queue[0][0]), Drupal.Eloqua.dataLookup.queue[0][1]]);
    }
    lookup[2].apply(this, [window.GetElqContentPersonalizationValue]);
  };

  /**
   * High level DataLookup.
   *
   * Handle change on inputs element and automatically update output elements with the result of each data lookup.
   *
   * Updating output elements is done by
   *  - Setting the value of ':input' elements.
   *  - Setting the HTML of ':not(:input)' elements.
   *
   * @param string guid
   *   The Data Lookup Key (aka. GUID).
   * @param inputs
   *   The inputs elements of the data lookup as a jQuery set. Each input element must have a field name attached as
   *   data under the 'eloqua-lookup-value-name' key.
   * @param outputs
   *   The outputs elements of the data lookup as an jQuery set. Each input element must have a field name attached as
   *   data under the 'eloqua-lookup-set-from' key.
   * @constructor
   */
  Drupal.Eloqua.DataLookup = function(guid, inputs, outputs) {
    this.guid = guid;
    this.inputs = inputs.filter(function(){return !!$.data(this, 'eloqua-lookup-value-name');});
    this.outputs = outputs.filter(function(){return !!$.data(this, 'eloqua-lookup-set-from');});
    // TODO: Display a spinner somewhere to inform user that something is happening in the background.
    $(inputs).change($.proxy(this, 'lookup'));
  };
  /**
   * Perform a data lookup, update outputs element when completed.
   */
  Drupal.Eloqua.DataLookup.prototype.lookup = function() {
    var value = '';
    var input_value, input_key;
    var inputs_length = this.input.length;
    for (var i = 0; i < inputs_length; i++) {
      input_value = this.inputs.get(i).val();
      // Input is not set, abort lookup.
      if (!input_value) return;
      input_key = $.data(this.inputs[i], 'eloqua-lookup-value-name');
      value += '<' + input_key + '>' + input_value + '</' + input_key + '>';
    }

    Drupal.Eloqua.dataLookup(this.guid, value, $.proxy(this, 'lookupComplete'));
  };
  /**
   * Process a complete data lookup.
   * @param function GetElqContentPersonalizationValue
   *   The GetElqContentPersonalizationValue from the datalookup.
   */
  Drupal.Eloqua.DataLookup.prototype.lookupComplete = function(GetElqContentPersonalizationValue) {
    this.output.filter(':input').each($.proxy(this, 'setInputValue', GetElqContentPersonalizationValue));
    this.output.filter(':not(:input)').each($.proxy(this, 'setElementHtml', GetElqContentPersonalizationValue));
  };
  Drupal.Eloqua.DataLookup.prototype.setElementHtml = function(element, GetElqContentPersonalizationValue) {
    $(element).html(GetElqContentPersonalizationValue($.data(element, 'eloqua-lookup-set-from')));
  };
  Drupal.Eloqua.DataLookup.prototype.setInputValue = function(element, GetElqContentPersonalizationValue) {
    $(element).val(GetElqContentPersonalizationValue($.data(element, 'eloqua-lookup-set-from')));
  };
})(jQuery);